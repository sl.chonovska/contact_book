from tkinter import *
from tkinter import ttk
import tkinter as tk
import sqlite3
from countries import Country
from tkinter import messagebox
import re


#get countries
countries = [x[1] for x in Country]

#see if valid email address
def isvalidEmail(email):
    pattern = "^\S+@\S+\.\S+$"
    objs = re.search(pattern, email)
    try:
        if objs.string == email:
            return True
    except:
        return False

root = tk.Tk()
root.geometry("1200x600")
root.title("Contact Book")


style=ttk.Style()
style.theme_use('clam')

conn = sqlite3.connect('contacts.db')

# add entry
def add_window():
    newWindow = Toplevel(root)
    # sets the title of the Toplevel widget
    newWindow.title("Add entry")
    # sets the geometry of toplevel
    newWindow.geometry("400x200")

    # add entry function
    def add_entry():
        conn = sqlite3.connect('contacts.db')
        c = conn.cursor() 

        if len(email_address.get()) == 0 or (email_address.get() and isvalidEmail(email_address.get())):
            val = (f_name.get(),l_name.get(),address.get(),city.get(),variable.get(),phone_number.get(), email_address.get())
        else:
            messagebox.showerror("Error with email","The inputed email address is invalid. Please enter again.")
            return
        try:
            c.execute("INSERT INTO addresses VALUES (:a, :b, :c, :d, :e, :f, :g)",
            {
                'a': val[0], 'b': val[1], 'c': val[2], 'd': val[3], 'e': val[4], 'f': val[5],'g': val[6]
            }
            )
            #commit changes
            conn.commit()
            #close connection
            conn.close()
            messagebox.showinfo("Entry Added","Entry added successfully!")
            newWindow.destroy()
            get_latest_data()
        except Exception as e:
            messagebox.showerror("Error",e)


    # All the buttons and texts
    f_name = Entry(newWindow,width=50)
    f_name.grid(row=0,column=1)
    Label(newWindow, text='First Name').grid(row=0,column=0)

    l_name = Entry(newWindow,width=50)
    l_name.grid(row=1,column=1)
    Label(newWindow, text='Last Name').grid(row=1,column=0)

    address = Entry(newWindow,width=50)
    address.grid(row=2,column=1)
    Label(newWindow, text='Address').grid(row=2,column=0)

    city = Entry(newWindow,width=50)
    city.grid(row=3,column=1)
    Label(newWindow, text='City').grid(row=3,column=0)
    
    # Create Dropdown menu
    variable = StringVar(newWindow)
    variable.set(countries[0]) # default value
    country = OptionMenu( newWindow ,variable ,*countries )
    country.grid(row=4,column=1)
    Label(newWindow, text='Country').grid(row=4,column=0)

    phone_number = Entry(newWindow,width=50)
    phone_number.grid(row=5,column=1)
    Label(newWindow, text='Phone Number').grid(row=5,column=0)

    email_address = Entry(newWindow,width=50)
    email_address.grid(row=6,column=1)
    Label(newWindow, text='Email Address').grid(row=6,column=0)

    confirmB = tk.Button(newWindow, text = "Add entry", command=add_entry, width=20)
    confirmB.grid(row=7,columnspan=2)

    newWindow.grab_set()  

# edit entry
def edit_window():

    # add edit function
    def edit_entry(id):
        conn = sqlite3.connect('contacts.db')
        c = conn.cursor() 

        #check for valid email address
        if len(email_address.get()) == 0 or (email_address.get() and isvalidEmail(email_address.get())):
            val = (f_name.get(),l_name.get(),address.get(),city.get(),variable.get(),phone_number.get(), email_address.get())
        else:
            messagebox.showerror("Error with email","The inputed email address is invalid. Please enter again.")
            return

        try:
            c.execute("UPDATE addresses SET first_name = :a, last_name = :b, address = :c, city = :d, country = :e, phone_number = :f, email_address = :g  WHERE oid = :id",
            {
                'a': val[0], 'b': val[1], 'c': val[2], 'd': val[3], 'e': val[4], 'f': val[5],'g': val[6], 'id': id
            }
            )
            #commit changes
            conn.commit()

            #close connection
            conn.close()
            messagebox.showinfo("Entry Edited","Entry edited successfully!")
            newEditWindow.destroy()
            get_newest_data()
        except Exception as e:
            messagebox.showerror("Error",e)

    #get the selected entry
    curItem = tree.focus()
    tree_item = tree.item(curItem)
    
    #if there is a selected entry
    if tree_item["values"]:
        #make new window
        newEditWindow = Toplevel(root)
        # sets the title of the Toplevel widget
        newEditWindow.title("Edit entry")
        # sets the geometry of toplevel
        newEditWindow.geometry("400x200")

        # All the buttons and texts
        f_name = Entry(newEditWindow,width=50)
        f_name.insert(0,tree_item["values"][1])
        f_name.grid(row=0,column=1)
        Label(newEditWindow, text='First Name').grid(row=0,column=0)

        l_name = Entry(newEditWindow,width=50)
        l_name.insert(0,tree_item["values"][2])
        l_name.grid(row=1,column=1)
        Label(newEditWindow, text='Last Name').grid(row=1,column=0)

        address = Entry(newEditWindow,width=50)
        address.insert(0,tree_item["values"][3])
        address.grid(row=2,column=1)
        Label(newEditWindow, text='Address').grid(row=2,column=0)

        city = Entry(newEditWindow,width=50)
        city.insert(0,tree_item["values"][4])
        city.grid(row=3,column=1)
        Label(newEditWindow, text='City').grid(row=3,column=0)
        
        # Create Dropdown menu
        variable = StringVar(newEditWindow)
        variable.set(tree_item["values"][5]) # default value
        country = OptionMenu( newEditWindow ,variable ,*countries )
        country.grid(row=4,column=1)
        Label(newEditWindow, text='Country').grid(row=4,column=0)

        phone_number = Entry(newEditWindow,width=50)
        phone_number.insert(0,tree_item["values"][6])
        phone_number.grid(row=5,column=1)
        Label(newEditWindow, text='Phone Number').grid(row=5,column=0)

        email_address = Entry(newEditWindow,width=50)
        email_address.insert(0,tree_item["values"][7])
        email_address.grid(row=6,column=1)
        Label(newEditWindow, text='Email Address').grid(row=6,column=0)

        confirmB = tk.Button(newEditWindow, text = "Edit entry", command=lambda : edit_entry(tree_item["values"][0]), width=20)
        confirmB.grid(row=7,columnspan=2)
        newEditWindow.grab_set()
    else:
        messagebox.showwarning("Edit Entry","No entry was selected. Go back and select and entry to edit.") 




# delete entry
def delete_entry():
    curItem = tree.focus()
    tree_item = tree.item(curItem)
    if tree_item['values']:
        msg = f"""Are you sure you want to delete entry:
        First name - {tree_item["values"][1]}
        Last name - {tree_item["values"][2]}
        Address - {tree_item["values"][3]}
        City - {tree_item["values"][4]}
        Country - {tree_item["values"][5]}
        Phone Number - {tree_item["values"][6]}
        Email Address - {tree_item["values"][7]}"""
        msgbox = messagebox.askokcancel("Delete Entry",msg)
        if msgbox:
            conn = sqlite3.connect('contacts.db')
            c = conn.cursor() 
            c.execute("DELETE FROM addresses WHERE oid=?", (tree_item["values"][0],))
            conn.commit()
            conn.close()

            get_newest_data()
        return
    msg = "You haven't selected anything. Select something and click the button again."
    messagebox.showinfo("Delete Entry",msg)

# cursor
c = conn.cursor() 

#table
c.execute('''CREATE TABLE IF NOT EXISTS addresses
    (
        first_name text NOT NULL,
        last_name text,
        address text,
        city text,
        country text,
        phone_number text,
        email_address text
    )
     ''')

#just quickly adding a new column to the existing table
#addColumn = "ALTER TABLE addresses ADD COLUMN email_address text"
#c.execute(addColumn)

# buttons
fm = Frame(root)
addB = tk.Button(root, text = "Add entry", command=add_window, width=20)
addB.pack()
editB = tk.Button(root, text = "Edit entry", command=edit_window, width=20)
editB.pack()
deleteB = tk.Button(root, text = "Delete entry", command=delete_entry, width=20)
deleteB.pack()
fm.pack(side = RIGHT)

# making the visual table
tree = ttk.Treeview(root, column=("c1", "c2", "c3","c4","c5","c6","c7","c8"), show='headings')
tree.column("#1", anchor=tk.CENTER,width=10)

tree.heading("#1", text="ID")

tree.column("#2", anchor=tk.CENTER, width=100)

tree.heading("#2", text="First Name")

tree.column("#3", anchor=tk.CENTER,width=100)

tree.heading("#3", text="Last Name")

tree.column("#4", anchor=tk.CENTER,width=100)

tree.heading("#4", text="Address")

tree.column("#5", anchor=tk.CENTER,width=100)

tree.heading("#5", text="City")

tree.column("#6", anchor=tk.CENTER,width=100)

tree.heading("#6", text="Country")

tree.column("#7", anchor=tk.CENTER,width=120)

tree.heading("#7", text="Phone Number")

tree.column("#8", anchor=tk.CENTER,width=150)

tree.heading("#8", text="Email Address")


tree.pack()


#getting the data after adding a new entry 
def get_latest_data():
    conn = sqlite3.connect('contacts.db')
    c = conn.cursor()
    c.execute("SELECT oid,* FROM addresses ORDER BY oid DESC LIMIT 1")
    record = c.fetchone()
    tree.insert("", tk.END, values=record)   

#getting the data after delete and edit
def get_newest_data():
    tree.delete(*tree.get_children())
    conn = sqlite3.connect('contacts.db')
    c = conn.cursor()
    c.execute("SELECT oid,* FROM addresses")
    records = c.fetchall()
    for record in records:
        tree.insert("", tk.END, values=record)       

# get initial records for table
c.execute("""SELECT oid, * from addresses""")
records = c.fetchall()
for record in records:
    tree.insert("", tk.END, values=record)  

#commit changes
conn.commit()

#close connection
conn.close()

root.mainloop()